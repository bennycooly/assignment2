package assignment2;
import java.util.*;

public class TurnsGame
//handles all turn-related things
{
	Game blackPegs(Game currentgame)
	{	//FIRST check for black pegs in exact spot
		//use stringbuilder for faster append (in case length of code increases
		StringBuilder tempcode = new StringBuilder();
		StringBuilder tempguess = new StringBuilder();
		for(int i = 0; i < currentgame.numPegs; i++)
		{
			String codeChecker1 = currentgame.code.substring(i,i+1);
			String codeChecker2 = currentgame.guess.substring(i,i+1);
			if(codeChecker1.equals(codeChecker2))
			{	//if match found, append a "-"
				tempcode.append('-');
				tempguess.append('-');
				currentgame.incrementBlack();
			}
			else
			{	//no match, so append what they were
				tempcode.append(codeChecker1);
				tempguess.append(codeChecker2);
			}
		}
		currentgame.guess = tempguess.toString();
		currentgame.code = tempcode.toString();
		return currentgame;
	}
	
	Game whitePegs(Game currentgame)
	{	//SECOND check white pegs
		for(int i = 0; i < currentgame.numPegs; i++)
		{
			String codeChecker1 = currentgame.guess.substring(i,i+1);
			while(codeChecker1.equals("-"))
			{	//skip all "-"
				i+=1;
				if(i<currentgame.numPegs){codeChecker1 = currentgame.guess.substring(i,i+1);}
				else{return currentgame;}
				
			}
			int indexofmatch = currentgame.code.indexOf(codeChecker1);
			if(indexofmatch >= 0)
			{	//if match found, replace the first instance with "-" in both code and guess
				currentgame.code = currentgame.code.replaceFirst(codeChecker1, "-");
				currentgame.guess = currentgame.guess.replaceFirst(codeChecker1, "-");
				currentgame.incrementWhite();
			}
		}
		return currentgame;
	}
}