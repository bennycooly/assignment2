/*
 * EE 422C Assignment 2 : Mastermind
 * Ben Fu and Patrick Haney
 * Lab Section: 2-3:30pm Thursday
 */
package assignment2;
import java.util.*;
public class Game
//main game class
{
	String posColors;//first letter of color, for new colors please add first letter capitalized
	int numPegs;//for a different number, change this initialization
	int numTurns;//for a different number, change this number
	//encapsulated variables
	private boolean mode; //change mode to be able to see the code
	private int numblackPegs = 0;
	private int numwhitePegs = 0;
	//variables modified by class only
	String origcode;
	String origguess;
	String code;
	String guess;
	ArrayList<String> guessHistory = new ArrayList<String>();
	Game(boolean mode1)
	{
		posColors = "BGOPRY";//first letter of color, for new colors please add first letter capitalized
		numPegs = 4;//for a different number, change this initialization
		numTurns = 12;//for a different number, change this number
		mode = mode1; 
	}
	void runGame(Game currentGame)
	{
		
		boolean cont=true;
		Prompts prompt = new Prompts();
		prompt.displayPrompts(1);	//starting prompts
		while(cont)
		{
			//initializations
			StartGame start = new StartGame();
			TurnsGame turn = new TurnsGame();
			prompt.displayPrompts(4);	//starting prompts
			if(!prompt.checkYN()){System.out.println("See you later!"); break;}
			currentGame = start.generateCode(currentGame);
			currentGame.origcode = currentGame.code;
			//turn
			while(currentGame.numTurns > 0)
			{
				currentGame.code = currentGame.origcode;	//get the original code again
				if(currentGame.mode){System.out.println(currentGame.code);}
				prompt.displayPrompts(1,currentGame);	//tell user how many guesses they have left
				boolean validGuess = true;
				while(validGuess)
				{
				prompt.displayPrompts(2);	//ask for next guess
				validGuess = prompt.getGuess(currentGame);	//get user input and validate
				}
				currentGame.origguess = currentGame.guess;//ensuring guess is saved
				currentGame = turn.blackPegs(currentGame);
				currentGame = turn.whitePegs(currentGame);
				prompt.displayPrompts(2, currentGame);	//display turn result 
				if(currentGame.numblackPegs==currentGame.numPegs){break;}
				guessHistory.add(origguess);//add current iteration to history
				guessHistory.add(Integer.toString(numwhitePegs));
				guessHistory.add(Integer.toString(numblackPegs));
				currentGame.numblackPegs = 0;
				currentGame.numwhitePegs = 0;
				currentGame.numTurns--;
			}
			if(currentGame.numTurns==0){System.out.println("Sorry, you ran out of turns. Better luck next time!");}
			prompt.displayPrompts(3);	//ask for reset
			cont = prompt.getReset();	//user inputs y/n for reset
			resetHandler();
		}
		
		
	}
	//following are getters and setters for numblackPegs and numwhitePegs
	void incrementBlack(){this.numblackPegs++;}
	int getBlack(){return this.numblackPegs;}
	void incrementWhite(){this.numwhitePegs++;}
	int getWhite(){return this.numwhitePegs;}
	void resetHandler()
	{
		numblackPegs=0; numwhitePegs=0; numTurns = 12;
	}
	void displayHistory()
	{//display tabular history of each guess
		for( int i = 0; i < guessHistory.size()/3; i++)
		{
			//format is Guess #: Code: Number of White Pegs: Number of Black Pegs
			System.out.print("Guess #"+(i+1));
			System.out.println(": "+guessHistory.get(i*3));
			System.out.print("Number of White Pegs: ");
			System.out.println(guessHistory.get(i*3 + 1));
			System.out.print("Number of Black Pegs: ");
			System.out.println(guessHistory.get(i*3 + 2));
			System.out.println("");
		}
	}
	public static void main(String[] args)
	{//initiates the game
		Game newgame = new Game(true);
		newgame.runGame(newgame);
	}
}


