package assignment2;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Prompts
//handles everything with user prompts and screen prints
{
	boolean checkYN()
	{
		Scanner input = new Scanner(System.in);
		String user;
		while(true)
		{
			user = input.nextLine();
			if(user.equals("Y")){return true;}
			else if(user.equals("N")){return false;}
			else{}
		}
	}
	void displayPrompts(int key) 	//basic prompts
	{
		switch (key) 
		{
		//print intro
		case 1: System.out.println("Welcome to Mastermind. Here are the rules. This is a text version of the classic board game Mastermind.\n"
				+ "The computer will think of a secret code. The code consists of 4 colored pegs.\n"
				+ "The pegs MUST be one of six colors: blue, green, orange, purple, red, or yellow.\n"
				+ "A color may appear more than once in the code. You try to guess what colored pegs are in the code and what order they are in.\n"
				+ "After you make a valid guess the result (feedback) will be displayed.\n"
				+ "The result consists of a black peg for each peg you have guessed exactly correct (color and position) in your guess.\n"
				+ "For each peg in the guess that is the correct color, but is out of position, you get a white peg.\n"
				+ "For each peg, which is fully incorrect, you get no feedback.\n"
				+ "Only the first letter of the color is displayed. B for Blue, R for Red, and so forth.\n"
				+ "When entering guesses you only need to enter the first character of each color as a capital letter.\n"
				+ "You have 12 guesses to figure out the secret code or you lose the game.");
			break;
		//ask for next guess
		case 2: System.out.print("What is your next guess? \nType in the characters for your guess and press enter.\nEnter guess: ");
			break;
		//ask for game prompt
		case 3: System.out.print("Are you ready for another game (Y/N): ");
			break;
		//ask for beginning ready prompt
		case 4: System.out.print("Are you ready to play? (Y/N): ");
			break;
		}
	}
	void displayPrompts(int key, Game currentGame)	//for prompts involving game variables
	{
		switch (key)
		{
		//display number of turns left
		case 1: System.out.println("You have "+currentGame.numTurns+" guesses left.");
			break;
		//display results with number of pegs
		case 2:
			System.out.print("Result: ");
			if(currentGame.getBlack()>0)
			{
				System.out.print(currentGame.getBlack()+" black peg(s)");
				if(currentGame.getWhite()>0){System.out.print(" and "+currentGame.getWhite()+" white peg(s)");}
			}
			else if(currentGame.getWhite()>0)
			{
				System.out.print(currentGame.getWhite()+" white peg(s)");
			}
			else{System.out.print("no pegs. Try again!");}
			if(currentGame.getBlack()==currentGame.numPegs){System.out.print("--You win!!!!");}
			System.out.println("");
		}
	}
	//guess handler
	boolean getGuess(Game currentGame)
	{
		Scanner input = new Scanner(System.in);
		String guess;
		guess = input.nextLine();
		if(guess.equals("history"))
		{
			//if user enters history, display the history
			currentGame.displayHistory();
			displayPrompts(2);
			guess = input.nextLine();
		}
		boolean y = validateInput(guess, currentGame);
		currentGame.guess = guess;
		return !y;
	}
	boolean validateInput(String input, Game currentGame)
	{
		if(input.length()!=currentGame.numPegs)
		 {	//first check for correct guess length
			System.out.println("INVALID GUESS");
			return false;
		 }
		for(int i=0;i<input.length();i++)
		{
			if(currentGame.posColors.indexOf(input.charAt(i))==-1)
			{	//if not a valid character, print error message
				System.out.println("INVALID GUESS");
				return false;
			}
		}
		return true;
	}
	
	//handling resets
	boolean getReset()
	{
		Scanner input = new Scanner(System.in);
		String reset;
		while(true)
		{
			reset = input.nextLine();
			if(reset.equals("Y")){return true;}
			else if(reset.equals("N")){return false;}
			else{}
		}
	}
}
