package assignment2;

import java.util.Random;

public class StartGame
//handles code generator
{
	StringBuilder tempCode = new StringBuilder();
	//generates a random code depending on number of colors and length of code
	Game generateCode(Game newgame)
	{
		for(int i = 0; i < newgame.numPegs; i++)
		{
			Random num = new Random();
			int num1 = num.nextInt() % newgame.posColors.length();
			if(num1<0){num1*=-1;}
			tempCode.append(newgame.posColors.charAt(num1));
		}
		newgame.code = tempCode.toString();
		return newgame;
	}
}
